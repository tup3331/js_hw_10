//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



let arr = ["travel", "hello", "eat", "ski", "lift"];

function countOfWords() {
    let filteredArr = arr.filter((word) => word.length > 3);
    return filteredArr.length;
}

console.log(countOfWords());



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



let arr1 = [
    {
        name: "Ivan",
        age: 25,
        sex: "male",
    },
    {
        name: "Anna",
        age: 23,
        sex: "female",
    },
    {
        name: "Alex",
        age: 27,
        sex: "male",
    },
    {
        name: "Luke",
        age: 24,
        sex: "female",
    }
]

function sexFilter(givenSex) {
    let filteredArr = arr1.filter((obj) => obj.sex === givenSex)
    return filteredArr;
}

console.log(sexFilter("male"));



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function filter(inArr, type) {
    let filteredArr = inArr.filter((obj) => typeof obj === type);
    return filteredArr;
}

console.log(filter(arr, `string`));



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



let arr2 = ['hello', 'world', 23, '23', null]

function filterBy(inArr, type) {
    let filteredArr = inArr.filter((obj) => typeof obj !== type);
    return filteredArr;
}

console.log(filterBy(arr2, `string`));